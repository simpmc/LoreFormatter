package me.typical.loreformatter.stats;

import me.typical.loreformatter.utils.configManager;
import me.typical.loreformatter.utils.itemManipulation;
import net.Indyuce.mmoitems.api.item.build.ItemStackBuilder;
import net.Indyuce.mmoitems.api.item.build.MMOItemBuilder;
import net.Indyuce.mmoitems.stat.data.BooleanData;
import net.Indyuce.mmoitems.stat.type.BooleanStat;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class EnchantHub extends BooleanStat {

    public EnchantHub() {
        super("ENCHANTHUB",
                Material.EXPERIENCE_BOTTLE,
                "Enchantment Hub",
                new String[]{
                        "Enable enchantments lines",
                        "in the lore."
                },
                new String[] {"all"});
    }
    @Override
    public void whenApplied(@NotNull MMOItemBuilder builder, @NotNull BooleanData statData) {
        ItemStack item = builder.getItemStack();
        HashMap<String, Integer> enchantments = new itemManipulation().getItemEnchantments(item);
        // Case 1: Item has enchantments
        if (enchantments.size() > 0) {

            builder.getLore().insert(getPath(), configManager.translate("&7Enchantments: &a"));
            for (String i : enchantments.keySet()) {
                System.out.println(enchantments.get(i));
                builder.getLore().insert(getPath(), "&f- &a" + i + " &f" + enchantments.get(i));
            }
        }
        // Case 2: Item has no enchantments
        else {
            builder.getLore().insert(getPath(), configManager.translate("&7Enchantments: &cNone"));
        }
    }
}