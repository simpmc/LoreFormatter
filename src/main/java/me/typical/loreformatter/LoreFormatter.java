package me.typical.loreformatter;

import me.typical.loreformatter.commands.debugCommand;
import me.typical.loreformatter.stats.EnchantHub;
import me.typical.loreformatter.utils.configManager;
import net.Indyuce.mmoitems.MMOItems;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public final class LoreFormatter extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        if (getServer().getPluginManager().getPlugin("MMOItems") != null) {
            getLogger().info("MMOItems found! Enabling hooks...");
            MMOItems.plugin.getStats().register(new EnchantHub());
        } else {
            getLogger().warning("MMOItems not found! Disabling plugin...");
            getServer().getPluginManager().disablePlugin(this);
        }
        Objects.requireNonNull(getCommand("itemdebug")).setExecutor(new debugCommand());
        configManager.loadlang();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
