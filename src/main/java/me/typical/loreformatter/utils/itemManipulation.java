package me.typical.loreformatter.utils;

import de.tr7zw.changeme.nbtapi.NBTItem;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

import static net.advancedplugins.ae.api.AEAPI.getEnchantmentsOnItem;

public class itemManipulation {

        public HashMap<String, Integer> getItemEnchantments(ItemStack item) {
        HashMap<String, Integer> enchantmentList = new HashMap<>();
        NBTItem nbtItem = new NBTItem(item);
        // Get the item Vanilla enchantments
        for (Enchantment enchantment : item.getEnchantments().keySet()) {
            enchantmentList.put(enchantment.getKey().getKey(), item.getEnchantmentLevel(enchantment));
        }
        // Get the item AdvancedEnchantments enchantments
        enchantmentList.putAll(getEnchantmentsOnItem(item));

        return enchantmentList;
    }
}
