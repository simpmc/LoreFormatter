package me.typical.loreformatter.utils;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class configManager {
    public static File locate = new File("plugins/LoreFormatter/", "config.yml");

    public static FileConfiguration DataFile = (FileConfiguration) YamlConfiguration.loadConfiguration(locate);
    public static void loadlang() {
        addlang("Support.AdvancedEnchantments", "true");
        try {
            DataFile.save(locate);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void save() {
        try {
            DataFile.save(locate);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getarray(String s) {
        if (DataFile.contains(s)) {
            List<String> ss = new ArrayList<>();
            for (String ok : DataFile.getStringList(s))
                ss.add(translate(ok));
            return ss;
        }
        return null;
    }

    public static int getint(String s) {
        if (DataFile.contains(s))
            return DataFile.getInt(s);
        return 0;
    }

    public static double getdoubl(String s) {
        if (DataFile.contains(s))
            return DataFile.getDouble(s);
        return 0.0D;
    }

    public static boolean getb(String s) {
        if (DataFile.contains(s))
            return DataFile.getBoolean(s);
        return false;
    }

    public static String getlang(String s) {
        if (DataFile.contains(s))
            return translate(DataFile.getString(s));
        return s;
    }

    public static void addlang(String s, double s2) {
        if (!DataFile.contains(s))
            DataFile.set(s, Double.valueOf(s2));
    }

    public static void addlang(String s, Boolean s2) {
        if (!DataFile.contains(s))
            DataFile.set(s, s2);
    }

    public static void addlang(String s, List<String> s2) {
        if (!DataFile.contains(s))
            DataFile.set(s, s2);
    }

    public static void setforcelang(String s, String s2) {
        DataFile.set(s, s2);
        save();
    }

    public static void setforcelang(String s, double x) {
        DataFile.set(s, Double.valueOf(x));
        save();
    }

    public static void addlang(String s, String s2) {
        if (!DataFile.contains(s))
            DataFile.set(s, s2);
    }

    public static void addlang(String s, int s2) {
        if (!DataFile.contains(s))
            DataFile.set(s, Integer.valueOf(s2));
    }

    public static void reload() {
        try {
            DataFile.load(locate);
        } catch (IOException|org.bukkit.configuration.InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
    public static String translate(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }
}
