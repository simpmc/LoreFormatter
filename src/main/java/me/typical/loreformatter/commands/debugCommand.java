package me.typical.loreformatter.commands;

import me.typical.loreformatter.utils.itemManipulation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import static me.typical.loreformatter.utils.configManager.translate;

public class debugCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (commandSender instanceof Player) {

            Player p = (Player) commandSender;
            if (p.hasPermission("loreformatter.itemdebug") || p.isOp()) {
                ItemStack item = p.getInventory().getItemInMainHand();
                HashMap<String, Integer> enchants = new itemManipulation().getItemEnchantments(item);
                for (String i : enchants.keySet()) {
                    p.sendMessage(translate("&f- &a" + i + " &f" + enchants.get(i)));
                }
                return true;
            }
            return true;

        }
        return true;
    }
}
